package unigame;

import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.Map; 
import java.util.TreeMap; 
import java.util.EnumMap; 
import java.util.LinkedList; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class UniGame extends mqapp.MQApp {

    static public void main(String[] passedArgs) {
        runSketch(appletArgs, new UniGame());
    }


public void setup(){
  size((int)(240*Globals.scale), (int)(160*Globals.scale));
  frameRate(30);
  Globals.textDisplayFont = createFont(Globals.textFontPath,Globals.scale*8);
  Globals.battleUIFont = createFont(Globals.battleFontPath,Globals.scale*8);
  Globals.battleManager = new BattleManager(this);
  Globals.player = new Player("p_"+Globals.playerType, this);
  Globals.attacks = new Attacks();
  Globals.battleUnits = new BattleUnits(this);
  Globals.gameStateManager = new GameStateManager();
  Globals.keyPressManager = new KeyPressManager(this);
  Globals.textManager = new TextManager(this);
  Globals.fadeManager = new FadeManager(this);
  Globals.world = new OverworldManager(this);

  //When the game is done setting up, we start the opening cutscene
  cutsceneOnePartZero();
}


public void draw(){
  Globals.gameStateManager.nextState();
}

public void keyPressed(){
  Globals.keyPressManager.setKey(keyCode);
}

public void keyReleased(){
  Globals.keyPressManager.removeKey(keyCode); 
}

PGraphics lastScene;

//custom per-pixel upscaling. Processing's default upscaler is super ugly with pixel art
public PImage upscale(PImage img) {
  img.loadPixels();
  PImage outimg = createImage(img.width*Globals.scale,img.height*Globals.scale,ARGB);
  outimg.loadPixels();
  for(int i = 0; i < img.height; ++i){
    for(int j = 0; j < img.width; ++j){
      for(int k = 0; k < Globals.scale; ++k){
        for(int l = 0; l < Globals.scale; ++l){
          outimg.pixels[i*outimg.width*Globals.scale+j*Globals.scale+k*outimg.width+l] = img.pixels[i*img.width+j];
        }
      }
    }
  }
  outimg.updatePixels();
  return outimg;
}

public void cutsceneOnePartZero() {
  Globals.textManager.printText(
    new String[]{"WELCOME TO UNIGAME! Press the Space Bar to continue."}, 
    new Lambda(){
      public void activate(){
        cutsceneOnePartOne();
}
}
);
}

public void cutsceneOnePartOne() {
  Globals.textManager.printText(
    new String[]{"TEACHER: Alright, just a quick reminder that assignment 1 is still due by 5pm today. Any questions?", 
    "KAREN: There's an assignment?"}, 
    new Lambda(){
      public void activate(){
        cutsceneOnePartTwo();
}
}
);
}

public void cutsceneOnePartTwo() {
  //Karen faces up
  //Really dirty way of doing this but we can guarantee karen is there so it works
  Globals.world.pcLab.tiles[3][7].setAppearance(loadImage(Globals.assetspath+"Characters/Karen/karen_up.png"), this);
  Globals.world.drawOverworld();
  Globals.world.sleepWorld(15, new Lambda() {
    public void activate() {
      cutsceneOnePartThree();
    }
  }
  );
}

public void cutsceneOnePartThree() {
  Globals.textManager.printText(
    new String[]{
    "KAREN: Can I please get an extension?", 
    "TEACHER: No, I think you can do it in time. Just follow the lectures.", 
    "KAREN: Uh, there were lectures?",  
    "TEACHER: …"
    }, 
    new Lambda(){
      public void activate(){
        cutsceneOnePartFour();
}
});
}

public void cutsceneOnePartFour() {
  Globals.world.pcLab.tiles[8][6].setAppearance(loadImage(Globals.assetspath+"Characters/Dev/dev_up.png"), this);
  Globals.world.drawOverworld();
  Globals.world.sleepWorld(15, new Lambda() {
    public void activate() {
      cutsceneOnePartFive();
    }
  }
  );
}

public void cutsceneOnePartFive() {
  Globals.textManager.printText(
    new String[]{
    "DEV: I want an HD!!!!!", 
    "TEACHER: That’s the spirit. Class is over, I look forward to seeing your answers."
    }, 
    new Lambda(){
      public void activate(){
        cutsceneOnePartSix();
}
});
}

public void cutsceneOnePartSix() {
  Globals.world.currentRoom.tiles[8][6].setAppearance(loadImage(Globals.assetspath+"Characters/Dev/dev_left.png"), this);
  Globals.world.drawOverworld();
  Globals.world.sleepWorld(15, new Lambda() {
    public void activate() {
      cutsceneOnePartSeven();
    }
  }
  );
}

public void cutsceneOnePartSeven() {
  Globals.world.pcLab.tiles[3][7].setAppearance(loadImage(Globals.assetspath+"Characters/Karen/karen_left.png"), this);
  Globals.world.drawOverworld();
  Globals.world.sleepWorld(15, new Lambda() {
    public void activate() {
      cutsceneOnePartEight();
    }
  }
  );
}

public void cutsceneOnePartEight() {
  Globals.textManager.printText(new String[]{
    "(I better start that assignment now. Navigate using the W,S,A,D keys and find your way out of the classroom.)"
    });
}









}
